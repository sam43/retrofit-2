package com.sam43.retrofit2practice;

/**
 * Created by sam43 on 5/7/17.
 */

class ApiRepo {
    private String name;

    public ApiRepo(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
