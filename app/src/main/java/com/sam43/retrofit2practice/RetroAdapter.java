package com.sam43.retrofit2practice;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by sam43 on 5/7/17.
 */

public class RetroAdapter extends ArrayAdapter<ApiRepo> {


    private Context context;
    private List<ApiRepo> values;

    public RetroAdapter(Context context, List<ApiRepo> values) {
        super(context, android.R.layout.simple_list_item_2 , values);

        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;

        if (row == null) {
            LayoutInflater inflater =
                    (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(android.R.layout.simple_list_item_2, parent, false);
        }

        TextView textView = (TextView) row.findViewById(android.R.id.text1);

        ApiRepo item = values.get(position);
        String message = item.getName();
        textView.setText(message);

        return row;
    }
}
