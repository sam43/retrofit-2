package com.sam43.retrofit2practice;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    private ListView listView;
    String API_BASE_URL = "https://api.github.com/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = (ListView) findViewById(R.id.list_view);

        Retrofit.Builder builder =
                new Retrofit.Builder()
                        .baseUrl(API_BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = builder.build();

        ApiClient client = retrofit.create(ApiClient.class);

        Call<List<ApiRepo>> call = client.repoForUser("sam43");

        call.enqueue(new Callback<List<ApiRepo>>() {
            @Override
            public void onResponse(Call<List<ApiRepo>> call, Response<List<ApiRepo>> response) {
                List<ApiRepo> repos = response.body();

                listView.setAdapter(new RetroAdapter(MainActivity.this, repos));
            }

            @Override
            public void onFailure(Call<List<ApiRepo>> call, Throwable t) {
                Toast.makeText(MainActivity.this, "Failed!", Toast.LENGTH_SHORT).show();
            }
        });

    }
}
