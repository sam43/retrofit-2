package com.sam43.retrofit2practice;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by sam43 on 5/7/17.
 */

public interface ApiClient {

    @GET("/users/{user}/repos")
    Call<List<ApiRepo>> repoForUser (@Path("user") String user);
}
